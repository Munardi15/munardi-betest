const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    fullName: { type: String, required: true },
    accountNumber: { type: Number, required: true },
    emailAddress: { type: String, required: true },
    registrationNumber: { type: String, required: true }
});

module.exports = mongoose.model('User', userSchema);
