const express = require("express");
const {
  createUser,
  getAllUsers,
  getUserById,
  updateUser,
  deleteUser,
  getUserByAccountNumber,
  getUserByRegistrationNumber,
  getAccountsByLastLogin,
} = require("../controller/userController");
const { cacheUser, cacheAllUsers, cacheUserByAccountNumber, cacheUserByRegistrationNumber, cacheAccountsByLastLogin } = require("../middleware/cache");
const { generateToken, authenticateToken }= require("../middleware/auth");
const router = express.Router();
router.post("/auth", (req, res) => {
  const username = req.body.username;
  const password = req.body.password;

  if (username === "admin" && password === "admin123") {
    const token = generateToken({ username });
    return res.json({ token });
  } else {
    return res.status(401).json({ message: "Invalid credentials" });
  }
});

router.post("/", authenticateToken, createUser);
router.get("/", authenticateToken,cacheAllUsers, getAllUsers);
router.get("/:id", authenticateToken,cacheUser, getUserById);
router.get("/account/:accountNumber", authenticateToken,cacheUserByAccountNumber, getUserByAccountNumber);
router.get("/registration/:registrationNumber",authenticateToken, cacheUserByRegistrationNumber, getUserByRegistrationNumber);
router.get("/accounts/lastlogin", authenticateToken, cacheAccountsByLastLogin, getAccountsByLastLogin);
router.put("/:id", authenticateToken, updateUser);
router.delete("/:id", authenticateToken, deleteUser);

module.exports = router;
