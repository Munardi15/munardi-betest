const { redisClient, connectRedis } = require("../config/redis");
const { promisify } = require("util");

const getAsync = promisify(redisClient.get).bind(redisClient);

// Middleware cache for individual user
const cacheUser = async (req, res, next) => {
    try {
        await connectRedis(); // Ensure Redis is connected
        const { id } = req.params;
        const data = await getAsync(id);
        if (data !== null) {
            res.send(JSON.parse(data));
        } else {
            next();
        }
    } catch (error) {
        console.error("Error caching user:", error);
        next(error);
    }
};

// Middleware cache for all users
const cacheAllUsers = async (req, res, next) => {
    try {
        await connectRedis(); // Ensure Redis is connected
        const data = await getAsync("allUsers");
        if (data !== null) {
            res.send(JSON.parse(data));
        } else {
            next();
        }
    } catch (error) {
        console.error("Error caching all users:", error);
        next(error);
    }
};
// Middleware cache for user by account number
const cacheUserByAccountNumber = async (req, res, next) => {
    try {
        await connectRedis(); // Ensure Redis is connected
        const accountNumber = req.params.accountNumber;
        const data = await getAsync(`user:accountNumber:${accountNumber}`);
        if (data !== null) {
            res.send(JSON.parse(data));
        } else {
            next();
        }
    } catch (error) {
        console.error("Error caching user by account number:", error);
        next(error);
    }
};

// Middleware cache for user by registration number
const cacheUserByRegistrationNumber = async (req, res, next) => {
    try {
        await connectRedis(); // Ensure Redis is connected
        const registrationNumber = req.params.registrationNumber;
        const data = await getAsync(`user:registrationNumber:${registrationNumber}`);
        if (data !== null) {
            res.send(JSON.parse(data));
        } else {
            next();
        }
    } catch (error) {
        console.error("Error caching user by registration number:", error);
        next(error);
    }
};

// Middleware cache for accounts by last login
const cacheAccountsByLastLogin = async (req, res, next) => {
    try {
        await connectRedis(); // Ensure Redis is connected
        const data = await getAsync("accountsByLastLogin");
        if (data !== null) {
            res.send(JSON.parse(data));
        } else {
            next();
        }
    } catch (error) {
        console.error("Error caching accounts by last login:", error);
        next(error);
    }
};

module.exports = {
    cacheUser,
    cacheAllUsers,
    cacheUserByAccountNumber,
    cacheUserByRegistrationNumber,
    cacheAccountsByLastLogin,
};