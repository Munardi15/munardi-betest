const jwt = require('jsonwebtoken');
const secretKey = process.env.JWT_SECRET
const generateToken = (payload) => {
    return jwt.sign(payload, secretKey);
};

const authenticateToken = (req, res, next) => {
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1];
    if (!token) {
        return res.status(401).json({ message: 'Unauthorized' });
    }

    jwt.verify(token, secretKey, (error, user) => {
        if (error) {
            return res.status(403).json({ message: 'Invalid token' });
        }
        req.user = user;
        next();
    });
};

module.exports = { generateToken, authenticateToken };