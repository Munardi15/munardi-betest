const User = require("../model/userInfo");
const AccountLogin = require('../model/accountLogin');
const { redisClient, connectRedis } = require("../config/redis");

// Function to get all users from database
const getAllUsersFromDatabase = async () => {
  return await User.find();
};

// Get all users
const getAllUsers = async (req, res) => {
  try {
    await connectRedis(); // Ensure Redis is connected
    const cachedUsers = await redisClient.get("allUsers");

    // Check if data is cached in Redis
    if (cachedUsers) {
      return res.json(JSON.parse(cachedUsers));
    }

    // If timeout occurs or data not found in Redis, fetch data from database
    const users = await getAllUsersFromDatabase();
    redisClient.set("allUsers", JSON.stringify(users));
    res.json(users);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
};

// Get user by ID
const getUserById = async (req, res) => {
  try {
    await connectRedis(); // Ensure Redis is connected
    const cachedUser = await redisClient.get(req.params.id);

    // Check if user data is cached in Redis
    if (cachedUser) {
      return res.json(JSON.parse(cachedUser));
    }

    // If timeout occurs or user data not found in Redis, fetch data from database
    const user = await User.findById(req.params.id);
    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }

    redisClient.set(req.params.id, JSON.stringify(user));
    res.json(user);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
};

// Create a new user
const createUser = async (req, res) => {
  try {
    const user = new User(req.body);
    await user.save();
    await connectRedis(); // Ensure Redis is connected
    redisClient.del("allUsers"); // Invalidate cache
    res.status(201).json(user);
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

// Update user by ID
const updateUser = async (req, res) => {
  try {
    const user = await User.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
    });
    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }
    await connectRedis(); // Ensure Redis is connected
    redisClient.set(req.params.id, JSON.stringify(user));
    redisClient.del("allUsers"); // Invalidate cache
    res.json(user);
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

// Delete user by ID
const deleteUser = async (req, res) => {
  try {
    const user = await User.findByIdAndDelete(req.params.id);
    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }
    await connectRedis(); // Ensure Redis is connected
    redisClient.del(req.params.id);
    redisClient.del("allUsers"); // Invalidate cache
    res.json({ message: "User deleted" });
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
};

// Get user by account number
const getUserByAccountNumber = async (req, res) => {
  try {
    await connectRedis(); // Ensure Redis is connected
    const accountNumber = req.params.accountNumber;
    const cachedUser = await redisClient.get(`user:accountNumber:${accountNumber}`);

    // Check if user data is cached in Redis
    if (cachedUser) {
      return res.json(JSON.parse(cachedUser));
    }

    // If timeout occurs or user data not found in Redis, fetch data from database
    const user = await User.findOne({ accountNumber });
    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }

    redisClient.set(`user:accountNumber:${accountNumber}`, JSON.stringify(user));
    res.json(user);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
};

// Get user by registration number
const getUserByRegistrationNumber = async (req, res) => {
  try {
    await connectRedis(); // Ensure Redis is connected
    const registrationNumber = req.params.registrationNumber;
    const cachedUser = await redisClient.get(`user:registrationNumber:${registrationNumber}`);

    // Check if user data is cached in Redis
    if (cachedUser) {
      return res.json(JSON.parse(cachedUser));
    }

    // If timeout occurs or user data not found in Redis, fetch data from database
    const user = await User.findOne({ registrationNumber });
    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }

    redisClient.set(`user:registrationNumber:${registrationNumber}`, JSON.stringify(user));
    res.json(user);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
};

// Get accounts with last login > 3 days
const getAccountsByLastLogin = async (req, res) => {
  try {
    await connectRedis(); // Ensure Redis is connected
    const cachedAccounts = await redisClient.get("accountsByLastLogin");

    // Check if data is cached in Redis
    if (cachedAccounts) {
      return res.json(JSON.parse(cachedAccounts));
    }

    // If timeout occurs or data not found in Redis, fetch data from database
    const threeDaysAgo = new Date(Date.now() - 3 * 24 * 60 * 60 * 1000);
    const accounts = await AccountLogin.find({ lastLoginDateTime: { $gt: threeDaysAgo } });
    redisClient.set("accountsByLastLogin", JSON.stringify(accounts));
    res.json(accounts);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
};

module.exports = {
  getAllUsers,
  getUserById,
  createUser,
  updateUser,
  deleteUser,
  getUserByAccountNumber,
  getUserByRegistrationNumber,
  getAccountsByLastLogin,
};
